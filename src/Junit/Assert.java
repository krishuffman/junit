package Junit;

import org.junit.Test;

import static org.junit.Assert.*;

public class Assert {
    @Test
    public void testAssertions(){
        //set up values to test
        String str = new String("Kris");
        String str1 = new String("Kris");
            String str2 = null;
            String str3 = "Kris";
            String str4 = "Kris";
            int val = 5;
            int val1 =6;
            String[] expectedArray = {"one", "two", "Three"};
            String[] resultArray = {"one", "two", "Three"};
            // are str and str1 euqal
            assertEquals(str, str1);
            // is the condition true
            assertTrue(val<val1);
            // is the condition false
            assertFalse(val1>val);
            //is the condition not null
            assertNotNull(str);
            // is the condition null
            assertNull(str2);
            // are the conditions the same
            assertSame(str3,str4);
            //are the conditions not the same
            assertNotSame(str,str1);
            //are the arrays equal
            assertArrayEquals(expectedArray,resultArray);

    }
}
